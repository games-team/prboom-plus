prboom-plus (2:2.6.2-1) unstable; urgency=medium

  * New upstream version 2.6.2
  * Remove all patches, backported from upstream.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 17 Feb 2022 08:29:59 +0100

prboom-plus (2:2.6.1um-1) unstable; urgency=medium

  * Fix debian/watch file.
  * New upstream version 2.6.1um
    + Implemented custom atexit() sequence, fixing crashes during
      shutdown e.g. when using Wayland (Closes: #988016).
  * Add some post-release patches from upstream to fix some minor
    issues with the build system.
  * Install binaries into /usr/games,
  * Significantly simplify packaging.
  * Bump Standards-Version to 4.5.1.
  * Add "Build-Depends: libasound2-dev [linux-any]" for the new ALSA
    music backend.
  * Bump "Build-Depends: libsdl2-dev" to (>= 2.0.7) as required by the
    build system.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 19 Aug 2021 21:10:45 +0200

prboom-plus (2:2.6um-1) unstable; urgency=medium

  * New upstream version 2.6um
  * Do not install glboom-plus anymore, it has been superseded by
    plain prboom-plus.
  * Install prboom2/NEWS as upstream changelog.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 09 Feb 2021 13:56:30 +0100

prboom-plus (2:2.5.1.7um+git127-1) unstable; urgency=medium

  * New upstream version 2.5.1.7um+git127
  * Adapt documentation install paths to upstream changes.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 01 Jan 2021 17:22:15 +0100

prboom-plus (2:2.5.1.7um+git102-1) unstable; urgency=medium

  * New upstream version 2.5.1.7um+git102
  * Adapt documentation install paths to upstream changes.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 03 Nov 2020 21:14:08 +0100

prboom-plus (2:2.5.1.7um+git89-1) unstable; urgency=medium

  * New upstream version 2.5.1.7um+git89
  * Remove config-debian.patch, all changes have been addressed
    upstream.
  * Replace all Suggests with ffmpeg, upstream has switched to an
    ffmpeg-based viddump approach.
  * Adapt packaging to upstream install rule.
  * Add usr/bin/prboom-plus to debian/not-installed, we install
    usr/bin/glboom-plus instead of prboom-plus.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 26 Jun 2020 09:37:17 +0200

prboom-plus (2:2.5.1.7um+git82-1) unstable; urgency=medium

  * New upstream version 2.5.1.7um+git82 from commit fa28d882.
    + Fix heap buffer overflows in UDP code (CVE-2019-20797),
      Closes: #961031.
    + Fix compilation on big-endian architectures,
      Closes: #961336.
  * Refresh config-debian.patch, the game speed changing keys
    have been unbound upstream.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 06 Jun 2020 15:09:36 +0200

prboom-plus (2:2.5.1.7um+git72-1) unstable; urgency=medium

  * Switch code base to the PrBoom+/UMAPINFO fork.
  * Adapt debian/watch file accordingly.
  * New upstream version 2.5.1.7um+git72 from commit e714a5bc.
  * Adapt debian/copyright accordingly.
  * Remove debian/gbp.conf.
  * Change default music backend back to SDL. OPL2 emulation is CPU
    expensive and MIDI rendering works with a default soundfont in SDL
    nowadays.
  * Add cmake to Build-Depends, upstream has changed the build
    system.
  * Adapt debian/rules and install rules to the changed build system.
  * Run "wrap-and-sort -asb".
  * No need to repack sources anymore, upstream has replaced or
    entirely removed all dubious or non-free data lumps.
  * Bump debhelper-compat to 13.
  * Bump Standards-Version to 4.5.0.
  * Rules-Requires-Root: no.
  * Strip "as-needed" LDFLAGS from debian/rules.
  * Remove obsolete Breaks and Replaces relations.
  * Install glboom-plus as prboom-plus.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 22 May 2020 16:33:38 +0200

prboom-plus (2:2.5.1.5+svn4540+dfsg1-2) unstable; urgency=medium

  * Remove myself from Uploaders.

 -- Jonathan Dowland <jmtd@debian.org>  Mon, 11 May 2020 10:14:12 +0100

prboom-plus (2:2.5.1.5+svn4540+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.5.1.5+svn4540+dfsg1
  * Unbind the Game Speed Up/Down keys, users might accidentally
    hit them (Closes: #922611).
  * Add timgm6mb-soundfont to Suggests as this is the default
    soundfont for the fluidsynth music backend (Closes: #925315).
  * In the default configuration, fix the sound font file name for
    the virtual sf3-soundfont-gm package. Change Suggests accordingly.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 05 Aug 2019 08:50:16 +0200

prboom-plus (2:2.5.1.5+svn4539+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.5.1.5+svn4539+dfsg1

 -- Fabian Greffrath <fabian@debian.org>  Sun, 13 Jan 2019 20:45:35 +0100

prboom-plus (2:2.5.1.5+svn4538+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.5.1.5+svn4538+dfsg1.
    + Fix network documentation TCP/IP -> UDP (Closes: #916201).
  * Fix vcs-obsolete-in-debian-infrastructure lintian warnings.
  * Fix insecure-copyright-format-uri lintian warning.
  * Bump dephelper compat to 11.
  * Bump Standards-Version to 4.2.1.

 -- Fabian Greffrath <fabian@debian.org>  Sun, 23 Dec 2018 21:30:24 +0100

prboom-plus (2:2.5.1.5+svn4531+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.5.1.5+svn4531+dfsg1.
    + DEL to clear key bindings in the setup menu (Closes: #827118).
  * Bump Build-Depends to the SDL2 versions of the libraries.
  * Bump debhelper compat to 10.
  * Enable all hardening flags.
  * Remove reproducible-build.patch, applied upstream.
  * Fix vcs-field-uses-insecure-uri lintian warnings.
  * Bump Standards-Version to 4.1.2.

 -- Fabian Greffrath <fabian@debian.org>  Wed, 03 Jan 2018 12:52:26 +0100

prboom-plus (2:2.5.1.5~svn4462+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 2.5.1.5~svn4462+dfsg1
    + This includes the 2.5.1.4 upstream release (r4459).
  * Restrict Build-Depends on libportmidi-dev to [linux-any].
  * Refresh patches.

 -- Fabian Greffrath <fabian@debian.org>  Sun, 24 Jan 2016 17:49:12 +0100

prboom-plus (2:2.5.1.4~svn4457+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 2.5.1.4~svn4457+dfsg1

 -- Fabian Greffrath <fabian@debian.org>  Mon, 30 Nov 2015 07:03:42 +0100

prboom-plus (2:2.5.1.4~svn4454+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 2.5.1.4~svn4454+dfsg1
  * Install bash-completion snippet.
  * Remove debian/patches/remove_google_adsense.patch,
    applied upstream.

 -- Fabian Greffrath <fabian@debian.org>  Wed, 28 Oct 2015 09:35:04 +0100

prboom-plus (2:2.5.1.4~svn4444+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 2.5.1.4~svn4444+dfsg1
  * Replace obsolete upstream version number with the
    Debian packaging version number.
  * Install prboom-plus.desktop file and SVG icon provided by upstream.
  * Drop Debian menu file and icons in PNG and XPM format.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 29 Sep 2015 08:52:35 +0200

prboom-plus (2:2.5.1.4~svn4433+dfsg1-1) unstable; urgency=medium

  * New upstream SVN snapshot.
  * Do not suggest soundfonts at all, leave this to libsdl-mixer1.2.
  * Register as a handler for the application/x-doom-wad MIME type.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 21 Sep 2015 06:57:19 +0200

prboom-plus (2:2.5.1.4~svn4429+dfsg1-1) unstable; urgency=medium

  * New upstream SVN snapshot.
  * Update Uploaders field with my Debian account.

 -- Fabian Greffrath <fabian@debian.org>  Thu, 27 Aug 2015 11:30:41 +0200

prboom-plus (2:2.5.1.4~svn4425+dfsg1-2) unstable; urgency=medium

  * Remove the transitional "prboom" package, add Breaks and Replaces
    to "prboom-plus" accordingly.
  * Remove virtual packages from Recommends, mention FreeDoom and
    FreeDM in the extended package description.
  * Turn debian/README.source into an actual shell script, turn the package
    name into a variable.
  * Update source location URI and Debian packaging copyright years in
    debian/copyright.
  * Remove menu entry for playing prboom-plus on a VC from
    debian/prboom-plus.menu.
  * Only install alternatives upon "abort-upgrade" or "configure" in
    debian/prboom-plus.postinst/prerm and add some more cosmetic fixes.
  * Remove obsolete forced xz compression of the Debian package.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 07 May 2015 15:59:54 +0200

prboom-plus (2:2.5.1.4~svn4425+dfsg1-1) unstable; urgency=medium

  * New upstream SVN snapshot.
  * Remove CPP timestamps to make builds reproducible.
  * Remove unused "Files:" paragraph from debian/copyright.
  * Add doc-base registration.
  * Bump Standards-Version to 3.9.6.
  * Condense Debian-specific changes to the default configuration
    into a single patch.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 30 Apr 2015 15:16:46 +0200

prboom-plus (2:2.5.1.4~svn4403+dfsg1-1) unstable; urgency=medium

  * New upstream SVN snapshot
    from new upstream SVN repository.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 23 Oct 2014 14:42:27 +0200

prboom-plus (2:2.5.1.4~svn4384+dfsg1-1) unstable; urgency=medium

  [ Fabian Greffrath ]
  * New upstream SVN snapshot.

  [ Vincent Cheng ]
  * Update to Standards version 3.9.5, no changes required.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Thu, 08 May 2014 09:55:46 +0200

prboom-plus (2:2.5.1.4~svn4356+dfsg1-1) unstable; urgency=low

  [ Fabian Greffrath ]
  * New upstream SVN snapshot.
  * Disable silent rules.
  * Add icons in SVG, PNG and XPM format, add icon entry to menu file,
    add desktop file (Closes: #726856).
  * Do not disable support for helper dogs anymore, we are using the
    free "baby worm demon" variant based on sprites and sounds from Freedoom.

  [ Jonathan Dowland ]
  * Remove google adsense tracking stuff from history.html that we distribute.
  * Update my name in the control file

 -- Jonathan Dowland <jmtd@debian.org>  Tue, 07 Jan 2014 21:39:37 +0000

prboom-plus (2:2.5.1.4~svn4346+dfsg1-1) unstable; urgency=low

  * New upstream SVN snapshot.
    + Mouse look now is available in software mode.
    + Added a crosshair. Three different crosshair graphics are for choice:
      cross, angle and dot. Extra features are changing crosshair colors
      according to the player's health and/or on sight of a target.
  * Add Build-Depends for all libraries we depend on, do not rely on libsdl*-dev
    to pull them in.
  * Demote freedoom et al. to Recommends to avoid circular dependencies.
  * Fix the sound font file name for musescore-soundfont-gm and prefer that
    package over freepats for usage with the fluidsynth MIDI player.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Tue, 10 Sep 2013 12:08:21 +0200

prboom-plus (2:2.5.1.4~svn4306+dfsg1-1) unstable; urgency=low

  * Upstream SVN snapshot of the prboom-plus codebase
    + Support up to eight joystick buttons instead of just four,
      the fifth and sixth buttons are mapped to strafe left and right
      (Closes: #606231, #612429).

  [ Jon Dowland ]
  * Initial packaging, based on prboom (Closes: #559132, #690905).
  * add a format specifier to a sprintf call

  [ Fabian Greffrath ]
  * Add myself to Uploaders.
  * The "--disable-i386-asm" configure flag is gone.
  * Append to LDFLAGS instead of overriding them.
  * Simplify the override_dh_auto_configure rule.
  * Add libdumb1-dev, libfluidsynth-dev, libportmidi-dev
    and libsdl-image1.2-dev to Build-Depends.
  * Add a symlink for the man page as well.
  * Remove obsolete debian/prboom+.files file.
  * Remove debian/prboom+.manpages, prboom-plus.6 is already
    installed by the build system.
  * Fix up binary and package names in debian/prboom+.{menu,prerm,postinst}.
  * Avoid redundant installation of docs.
  * Mention PrBoom+ in the extended package description.
  * Move "boom-wad | game-data-packager | doom-wad" to Depends, since we
    must have an IWAD in order to start the game.
  * Change default MIDI player to the OPL2 emulator, so "freepats" can get
    demoted to Suggests.
  * Add debian/README.source.
  * Provide a dummy package for the transition from prboom to prboom-plus.
  * Split prboom-plus-game-server into a separate package, prboom-plus
    links against way more libraries (Closes: #528057).
  * Simplify maintainer scripts.
  * Suggest mkvtoolnix, vorbis-tools and x264 for video capturing and
    change default Ogg Vorbis encoder name to oggenc.
  * Shorten and (hopefully) clarify package descriptions.

 -- Fabian Greffrath <fabian+debian@greffrath.com>  Wed, 19 Jun 2013 10:01:34 +0200
